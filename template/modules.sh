#!/usr/bin/env bash

# Load Initial Modules
# shellcheck disable=SC2086
module load $OOD_MODULES

# Setup Command Replacements
if which --skip-alias --skip-functions code-server &>/dev/null; then
	OOD_CODE_SERVER="$(which --skip-alias --skip-functions code-server)"
	export OOD_CODE_SERVER
	code-server () { "$OOD_CODE_SERVER" "$@" ; }
	export -f code-server
else
	echo "Error: 'code-server' not found in module environment! Stopping!!"
	exit 44
fi

# Reset module system for user environmennt setup script compatibility
module -q reset
