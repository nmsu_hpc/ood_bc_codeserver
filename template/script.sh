#!/usr/bin/env bash

# Ensure modules are set to default
module -q reset

# Prepare 'code-server' based on submit type
if [[ "$OOD_SUBMIT_TYPE" =~ ^sif.* ]]; then
	# shellcheck disable=SC1091
	source "${OOD_STAGED_ROOT}/sif.sh"
else
	# shellcheck disable=SC1091
	source "${OOD_STAGED_ROOT}/modules.sh"
fi

# Ensure Compatible Workspace
if [ ! -d "$CODE_SERVER_WORKSPACE" ]; then
	if [ ! -f "$CODE_SERVER_WORKSPACE" ] \
		|| ! grep -Eq '\.code-workspace$' <<< "$CODE_SERVER_WORKSPACE"
	then
		echo "Error: '$CODE_SERVER_WORKSPACE' is invalid! Using Default Workspace File!!"
		CODE_SERVER_WORKSPACE="${OOD_STAGED_ROOT}/job.code-workspace"
	fi
fi

# Setup dirs for code-server permanant data
CODE_SERVER_DATAROOT="$HOME/.local/share/code-server"
mkdir -p "$CODE_SERVER_DATAROOT/extensions"

# CD Into Home Directory
if [ -d "$CODE_SERVER_WORKSPACE" ]; then
	cd "$CODE_SERVER_WORKSPACE" || true
else
	cd "$HOME" || true
fi

# Source User's Environment Setup Secript
if [ -n "$OOD_ENV_SETUP_SCRIPT" ] && [ -f "$OOD_ENV_SETUP_SCRIPT" ]; then
	echo "Starting: Sourcing User Environment Setup Script"
	# shellcheck disable=SC1090
	source "$OOD_ENV_SETUP_SCRIPT"
	echo "Finished: Sourcing User Environment Setup Script"
elif [ -n "$OOD_ENV_SETUP_SCRIPT" ] && [ ! -f "$OOD_ENV_SETUP_SCRIPT" ]; then
	echo "Error: User environment setup script '$OOD_ENV_SETUP_SCRIPT' specified but does not exist! Skipping!!"
fi

# Validate code-server is in PATH or Function
if ! which code-server &>/dev/null; then
	"Error: 'code-server' is not in PATH or defined as a function! Stopping!!"
	exit 9
fi

# Start VSCode
echo -e "\nStarting Code Server:"
code-server \
	--auth="password" \
	--bind-addr="0.0.0.0:${PORT}" \
	--disable-telemetry \
	--disable-update-check \
	--extensions-dir="$CODE_SERVER_DATAROOT/extensions" \
	--user-data-dir="$CODE_SERVER_DATAROOT" \
	--ignore-last-opened \
	"$CODE_SERVER_WORKSPACE"
