#!/usr/bin/env bash

# Report SIF Image
echo ""
echo "The Following SIF Image Is Selected:"
echo "$OOD_SIF"
echo ""

# Setup Command Replacement
code-server () {
	if [ -z "$OOD_SIF" ]; then
		echo 'Error: ENV "SIF" is not set! Stopping!!'
		exit 15
	elif [ ! -f "$OOD_SIF" ]; then
		echo "Error: '$OOD_SIF' does not exist! Stopping!!"
		exit 16
	elif ! apptainer exec "$OOD_SIF" bash -c "command -v code-server &>/dev/null"; then
		echo "Error: 'code-server' is missing from the PATH inside the provided container! Stopping!!"
		exit 17
	elif [ -n "$CUDA_VISIBLE_DEVICES" ]; then
		apptainer exec --nv "$OOD_SIF" code-server "$@"
	else
		apptainer exec "$OOD_SIF" code-server "$@"
	fi
}
export -f code-server
